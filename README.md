# Squares
An attempt to solve the [Squaring the square](https://en.wikipedia.org/wiki/Squaring_the_square) problem with genetic algorythm
Use matplotlib to show result and evolution graph

Usage :
 - install matplotlib
 - `./main.py`
 - Untit testing : `./testing.py`
