#!/usr/bin/env python3


import unittest
from main import *



class TestHuman(unittest.TestCase):
    def test_adn1(self):
        pedro=Human([[0,0,2],[3,0,3]])
        self.assertEqual(pedro.adn,[[0,0,2],[3,0,3]])
        self.assertEqual(pedro.score(),13)

    def test_adn2(self):
        pedro=Human([[0,1,2],[1,0,2],\
                     [2,1,3],[4,0,1]])
        self.assertEqual(pedro.size,4)
        self.assertEqual(pedro.score(),10)
        self.assertEqual(pedro.score,10)

    def test_emptyadn(self):
        pedro=Human()
        self.assertIsInstance(pedro.adn[1][0], int)
        self.assertIsInstance(pedro.score(), int)
        self.assertEqual(pedro.size, len(pedro.adn))
        self.assertTrue( GLOBAL_ADN_INIT/2 -1 <= pedro.size <= GLOBAL_ADN_INIT +1)

class TestIntersects(unittest.TestCase):
    def test_intersect_linear(self):
        for i in [[(0,1,2,3),0],[(0,2,1,3),1],[(1,2,0,3),1],[(1,3,0,2),1],[(3,4,1,2),0],[(0,1,0,1),1],[(0,2,2,5),0]]:
            self.assertEqual(intersect_linear(i[0][0],i[0][1],i[0][2],i[0][3]) , i[1])

        with self.assertRaises(TypeError): intersect_linear(1,2,3)

    def test_intersect_square(self):
        for i in [[[0,1,2],[1,0,2],1], \
                  [[0,0,3],[1,1,4],4], \
                  [[0,1,2],[2,1,3],0] ]:
            self.assertEqual(intersect_square(i[0],i[1]) , i[2])

class TestNormalise(unittest.TestCase):
    def test_normalise_square(self):
        testlist=[  [[-1,0,2],            [0,0,2]],\
                    [[0,-1,3],            [0,0,3]],\
                    [[FIELD_SIZE-1, 5,2], [FIELD_SIZE-2,5,2]],\
                    [[3, FIELD_SIZE-2,3], [3,FIELD_SIZE-3,3]] ]
        for test,result in testlist:
            self.assertEqual(normalise_square(test),result)

if (__name__ == "__main__" ) :
    unittest.main()
