#!/usr/bin/env python3

import matplotlib.pylab as plt
import numpy as np
import time
import math
from random import randrange, choices
from matplotlib.patches import Rectangle
from statistics import mean,stdev,variance


FIELD_SIZE = 112
MAX_SIZE = 50 
GLOBAL_ADN_INIT = 30
MUTATION_RATE = 1
POPULATION = 30
pop=[]
gen=0
stats=[]

# One individual
class Human:
    adn = []
    adn_size=0
    score = 0
    def __init__(self, adn=0):

        if( isinstance(adn,list)):
            self.adn = adn
            self.size = len(adn)
        else:
            self.size=0
            self.adn=[]
            for i in range(randrange(int(GLOBAL_ADN_INIT/2),GLOBAL_ADN_INIT)):
                self.add_random_square()
            

    # Score is defined as follow :
    #   Total area covvered by all the squares - 4 * overlaping between squares
    def score(self):
        area=0
        for i in self.adn:
            area+=(i[2]**2)

        intersects=0
        for i in range(self.size):
            for j in range(i+1,self.size):
                intersects+=intersect_square(self.adn[i],self.adn[j])

        self.score = area - intersects * 4
        return self.score


    def add_random_square(self):
        s=randrange(MAX_SIZE)+1
        x,y=randrange(FIELD_SIZE-s+1),randrange(FIELD_SIZE-s+1)
        self.adn.append([x,y,s])
        self.size += 1



    def show(self,block=False):
        plt.figure(1)
        plt.clf()

        for i in self.adn:
            self.show_square(i)
        plt.plot([0,FIELD_SIZE,FIELD_SIZE,0,0], [FIELD_SIZE,FIELD_SIZE,0,0,FIELD_SIZE])

        # axnext = plt.axes([0, 0.00, 0.1, 0.05])
        # bnext = Button(axnext, 'Next')
        # bnext.on_clicked(CallbackNext)

        if(block):
            plt.show()
        else:
            plt.figure(1)
            # if buttons :
            plt.pause(0.001) 
        
    # Show the square with matplotlib
    def show_square(self, squaredata):
        x,y,s = squaredata
        plt.plot([x, x + s, x + s, x , x , x + s], [y + s, y + s, y, y, y + s, y])
        # plt.plot([x, x + s, x + s, x , x], [y + s, y + s, y, y, y + s])
        # plt.plot([x,x + s],[y, y + s])
        # plt.plot([x + s,x],[y, y + s])

    # randomly change some squares in the adn
    def mutate(self):
        newAdn=[]
        for squaredata in self.adn:
            x,y,s = squaredata
            if(randrange(100) < MUTATION_RATE):
                x += randrange(2)*2 - 1
                y += randrange(2)*2 - 1
                s += randrange(2)*2 - 1
            newAdn.append(normalise_square([ x,y,s ]))

        size=len(newAdn)
        if(randrange(100) < MUTATION_RATE and size > 3):
            id=randrange(size)
            del(newAdn[id])
            self.size -= 1

        self.adn = newAdn
        if(randrange(100) < MUTATION_RATE):
            self.add_random_square()

# if 2 segment a1-a2 and b1-b2, calculate the overlapping between theses. Used to calculate square intersect
def intersect_linear(a1,a2,b1,b2):
    if(b2<=a1 or b1>=a2): return 0
    if(b1<=a1):
        if(b2<a2):  return int(b2-a1)
        else:       return int(a2-a1)
    if(a1<b1 and b1<a2):
        if(b2<a2):  return int(b2-b1)
        else:       return int(a2-b1)
    print ('intersect_linear_error')

# calculate square intersection area
def intersect_square(s1,s2):
    xarea=int(intersect_linear(s1[0], s1[0]+s1[2], s2[0], s2[0]+s2[2]))
    yarea=int(intersect_linear(s1[1], s1[1]+s1[2], s2[1], s2[1]+s2[2]))
    return int(xarea)*int(yarea)

# normalise square to be sure it stay inside the area
def normalise_square(squaredata):
    x,y,s = squaredata
    s = max(s,1)
    s = min(s , MAX_SIZE)
    # Make sure that the square does not goes out of the field. If it is, reduce the radius.
    x=max(x,0)
    y=max(y,0)
    x=min(x,FIELD_SIZE-s)
    y=min(y,FIELD_SIZE-s)
    return([x,y,s])


def CallbackNext(arg):
    #print('callback received')
    #print(arg)
    #plt.cla()

    # plt.draw()
    # plt.close()
    print('callback')


def PopInit():

    global pop
    pop=[]
    gen = 0

    for i in range(POPULATION):
        pop.append(Human())
    # to test display and score :
    # pop[0].adn=[ [ 0, 0,28],[ 0,28,28],[ 0,56,28],[ 0,84,28],\
    #              [28, 0,28],[28,28,28],[28,56,28],[28,84,28],\
    #              [56, 0,28],[56,28,28],[56,56,28],[56,84,28],\
    #              [84, 0,28],[84,28,28],[84,56,28],[84,84,28] ]

# That is one generation 
def PopReproduce():
    global gen
    global pop
    #print(pop)
    gen+=1

    # Do random mutation and calculate scores
    for human in pop:
        # print(human.adn)
        human.mutate()
        # print(human.adn)
        human.score()

    pop.sort(key=lambda x:x.score,reverse=True)
    score_min=pop[-1].score
    score_max=pop[0].score
    stats.append(score_max)

    # Once every 100 generation, display graph and stats (faster evolution)
    if( gen % 100 == 0) :
        pop[0].show()
        print ("Generation {} score min : {} and max : {}".format(gen,score_min,score_max))
        plt.figure(2)
        plt.clf()
        plt.plot(range(gen),stats)
        plt.show(block=False)

    # Square the score to favorise highest scores
    scores = [(human.score - score_min) ** 2 for human in pop]

    new_pop = [Human(i.adn) for i in choices(pop,scores,k=POPULATION)]
    
    pop=new_pop


def main():
    global pop
    PopInit()          
    while(True):
        PopReproduce()

    return


if( __name__ == "__main__"):
    main()
